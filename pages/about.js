import React from 'react';
import AboutPageContent from "../components/AboutPageContent/AboutPageContent";

function About() {
    return (
        <AboutPageContent/>
    );
}

export default About;
