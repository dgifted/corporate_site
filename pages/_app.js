import React, {useRef} from "react";
// import {useRouter} from "next/router";

// import {LocomotiveScrollProvider} from 'react-locomotive-scroll';

import '/public/assets/css/vendor-styles.css';
import '/public/assets/css/theme-styles.css';
import '/public/assets/css/custom-styles.css';

// import "locomotive-scroll/dist/locomotive-scroll.min.css";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import '../styles/homepage.css';

function MyApp({Component, pageProps}) {
    // const containerRef = useRef(null);
    // const {asPath} = useRouter();

    return <Component {...pageProps} />
}

export default MyApp
