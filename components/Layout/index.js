import React from 'react';
import Head from "next/head";
import {useRouter} from "next/router";
import Navbar from "../Navbar";
import Footer from "../Footer";
import BackToTop from "../BackToTop";

function Layout({children, ...customMeta}) {
    const router = useRouter();
    const meta = {
        title: 'More Event',
        description: 'More event website',
        type: 'website',
        ...customMeta
    };

    return (
        <>
            <Head>
                <title>{meta.title}</title>
                <meta name="robots" content="follow, index" />
                <meta content={meta.description} name="description" />
                <meta
                    property="og:url"
                    content={`https://yourwebsite.com${router.asPath}`}
                />
                <link
                    rel="canonical"
                    href={`https://yourwebsite.com${router.asPath}`}
                />
                <meta property="og:type" content={meta.type} />
                <meta property="og:site_name" content="Manu Arora" />
                <meta property="og:description" content={meta.description} />
                <meta property="og:title" content={meta.title} />
                <meta property="og:image" content={meta.image} />
                {meta.date && (
                    <meta property="article:published_time" content={meta.date} />
                )}
                <link rel="shortcut icon" type="image/x-icon" href="/assets/img/favicon.ico" />
            </Head>
            <Navbar/>
            <main>{children}</main>
            <Footer/>
            <BackToTop/>
        </>
    );
}

export default Layout;
