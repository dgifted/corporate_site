import React from 'react';
import Layout from "../Layout";
import BrandAreaSection from "../BrandAreaSection";
import TestimonialsAreaSection from "../TestimonialsAreaSection";
import CountdownAreaSection from "../CountdownAreaSection";

function AboutPageContent() {
    return (
        <Layout title={`${process.env.siteName} | About Us`}>
            <div className="slider-area2">
                <div className="slider-height2 hero-overly2 d-flex align-items-center">
                    <div className="container">
                        <div className="row">
                            <div className="col-xl-12">
                                <div className="hero-cap hero-cap2 text-center">
                                    <h2>About Us</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="about-details section-padding30">
                <div className="container">
                    <div className="row">
                        <div className="offset-xl-1 col-lg-8">
                            <div className="about-details-cap mb-50">
                                <h4>Our Mission</h4>
                                <p>Consectetur adipiscing elit, sued do eiusmod tempor ididunt udfgt labore et dolore
                                    magna
                                    aliqua. Quis ipsum suspendisces gravida. Risus commodo viverra sebfd dho eiusmod
                                    tempor
                                    maecenas accumsan lacus. Risus commodo viverra sebfd dho eiusmod tempor maecenas
                                    accumsan
                                    lacus.
                                </p>
                                <p> Risus commodo viverra sebfd dho eiusmod tempor maecenas accumsan lacus. Risus
                                    commodo
                                    viverra sebfd dho eiusmod tempor maecenas accumsan.</p>
                            </div>
                            <div className="about-details-cap mb-50">
                                <h4>Our Vision</h4>
                                <p>Consectetur adipiscing elit, sued do eiusmod tempor ididunt udfgt labore et dolore
                                    magna
                                    aliqua. Quis ipsum suspendisces gravida. Risus commodo viverra sebfd dho eiusmod
                                    tempor
                                    maecenas accumsan lacus. Risus commodo viverra sebfd dho eiusmod tempor maecenas
                                    accumsan
                                    lacus.
                                </p>
                                <p> Risus commodo viverra sebfd dho eiusmod tempor maecenas accumsan lacus. Risus
                                    commodo
                                    viverra sebfd dho eiusmod tempor maecenas accumsan.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="support-company-area pt-100 pb-100 section-bg fix"
                 style={{ backgroundImage: `url("/assets/img/gallery/section_bg02.jpg.webp")`}}
            >
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-xl-6 col-lg-6">
                            <div className="support-location-img">
                                <img src="/assets/img/gallery/xabout.png.pagespeed.ic.NolByd-O8M.png" alt=""/>
                            </div>
                        </div>
                        <div className="col-xl-6 col-lg-6">
                            <div className="right-caption">

                                <div className="section-tittle section-tittle2 mb-50">
                                    <span>Our Top Services</span>
                                    <h2>Our Best Services</h2>
                                </div>
                                <div className="support-caption">
                                    <p className="pera-top">Mollit anim laborum duis adseu dolor iuyn voluptcate velit
                                        ess cillum
                                        dolore egru lofrre dsu quality mollit anim laborumuis au dolor in voluptate
                                        velit
                                        cillu.</p>
                                    <p className="mb-65">Mollit anim laborum.Dvcuis aute serunt iruxvfg dhjkolohr indd
                                        re voluptate
                                        velit esscillumlore eu quife nrulla parihatur. Excghcepteur sfwsignjnt occa
                                        cupidatat
                                        non aute iruxvfg dhjinulpadeserunt moll.</p>
                                    <a href="about.html" className="btn post-btn">More About Us</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="team-area section-padding30">
                <div className="container">
                    <div className="row">
                        <div className="cl-xl-7 col-lg-8 col-md-10">

                            <div className="section-tittle mb-70">
                                <span>Our Professional members </span>
                                <h2>Our Team Mambers</h2>
                            </div>
                        </div>
                    </div>
                    <div className="row">

                        <div className="col-xl-4 col-lg-4 col-md-6 col-sm-">
                            <div className="single-team mb-30">
                                <div className="team-img">
                                    <img src="/assets/img/gallery/xteam2.png.pagespeed.ic.ztMh4IVh87.png" alt=""/>
                                </div>
                                <div className="team-caption">
                                    <h3><a href="#">Ethan Welch</a></h3>
                                    <span>UX Designer</span>
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-4 col-lg-4 col-md-6 col-sm-">
                            <div className="single-team mb-30">
                                <div className="team-img">
                                    <img src="/assets/img/gallery/team3.png" alt=""/>
                                </div>
                                <div className="team-caption">
                                    <h3><a href="#">Ethan Welch</a></h3>
                                    <span>UX Designer</span>
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-4 col-lg-4 col-md-6 col-sm-">
                            <div className="single-team mb-30">
                                <div className="team-img">
                                    <img src="/assets/img/gallery/xteam1.png.pagespeed.ic.FQ42DN_v4L.png" alt=""/>
                                </div>
                                <div className="team-caption">
                                    <h3><a href="#">Ethan Welch</a></h3>
                                    <span>UX Designer</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <TestimonialsAreaSection/>

            <CountdownAreaSection/>

            <BrandAreaSection extraClasses="pt-150"/>
        </Layout>
    );
}

export default AboutPageContent;
