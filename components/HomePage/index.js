import React, {useEffect, useState} from 'react';
import Slider from "react-slick";
import CountUp from 'react-countup';
import VisibilitySensor from 'react-visibility-sensor';

import Layout from "../Layout";
import BrandAreaSection from "../BrandAreaSection";
import TestimonialsAreaSection from "../TestimonialsAreaSection";
import CountdownAreaSection from "../CountdownAreaSection";

const settings = {
    dots: false,
    autoplay: true,
    infinite: true,
    speed: 1000,
    slidesToShow: 1,
    slidesToScroll: 1
};

function HomePageContent() {
    const [mounted, setMounted] = useState(false);

    useEffect(() => {
        setMounted(true);
    }, []);


    return (
        <Layout title={`${process.env.siteName} | Home page`}>
            <div className="slider-area"
                 style={{backgroundImage: `url("/assets/img/hero/xh1_hero.jpg.pagespeed.ic.Sn2ey8kriY.webp")`}}>
                <div className="slider-active">
                    <Slider {...settings}>
                        <div className="single-slider slider-height d-flex align-items-center">
                            <div className="container">
                                <div className="row">
                                    <div className="col-xl-8 col-lg-7 col-md-8">
                                        <div className="hero__caption">
                                            <span data-animation="fadeInLeft"
                                                  data-delay=".1s">Committed to excellence</span>
                                            <h1 data-animation="fadeInLeft" data-delay=".5s">Events coverage, second to none</h1>
                                            <p data-animation="fadeInLeft" data-delay=".9s">Mollit anim laborum.Dvcuis
                                                aute
                                                serunt
                                                iruxvfg dhjkolohr indd re voluptate<br/> velit esscillumlore eu quife
                                                nrulla
                                                parihatur.</p>

                                            <div className="hero__btn" data-animation="fadeInLeft" data-delay="1.1s">
                                                <a href="#" className="btn hero-btn">Our Services</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="single-slider slider-height d-flex align-items-center">
                            <div className="container">
                                <div className="row">
                                    <div className="col-xl-8 col-lg-7 col-md-8">
                                        <div className="hero__caption">
                                            <span data-animation="fadeInLeft"
                                                  data-delay=".1s">Committed to excellence</span>
                                            <h1 data-animation="fadeInLeft" data-delay=".5s">We help to reach your
                                                business goals</h1>
                                            <p data-animation="fadeInLeft" data-delay=".9s">Mollit anim laborum.Dvcuis
                                                aute
                                                serunt
                                                iruxvfg dhjkolohr indd re voluptate<br/> velit esscillumlore eu quife
                                                nrulla
                                                parihatur.</p>

                                            <div className="hero__btn" data-animation="fadeInLeft" data-delay="1.1s">
                                                <a href="#" className="btn hero-btn">Our Services</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Slider>

                </div>
            </div>


            <div className="categories-area section-padding30">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">

                            <div className="section-tittle mb-70">
                                <span>Our Top Services</span>
                                <h2>Our Best Services</h2>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-4 col-md-6 col-sm-6">
                            <div className="single-cat text-center mb-50">
                                <div className="cat-icon">
                                    <span className="flaticon-development"/>
                                </div>
                                <div className="cat-cap">
                                    <h5><a href="#">Events Coverage</a></h5>
                                    <p>There are many variations of passages of lorem Ipsum available but the new
                                        majority have
                                        suffered.</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-6">
                            <div className="single-cat text-center mb-50">
                                <div className="cat-icon">
                                    <span className="flaticon-result"/>
                                </div>
                                <div className="cat-cap">
                                    <h5><a href="#">ICT Solutions</a></h5>
                                    <p>There are many variations of passages of lorem Ipsum available but the new
                                        majority have
                                        suffered.</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-6">
                            <div className="single-cat text-center mb-50">
                                <div className="cat-icon">
                                    <span className="flaticon-team"/>
                                </div>
                                <div className="cat-cap">
                                    <h5><a href="#">Wealth Investments</a></h5>
                                    <p>There are many variations of passages of lorem Ipsum available but the new
                                        majority have
                                        suffered.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div className="support-company-area pt-100 pb-100 section-bg fix"
                 style={{ backgroundImage: `url("assets/img/gallery/section_bg02.jpg.webp")`}}
            >
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-xl-6 col-lg-6">
                            <div className="support-location-img">
                                <img src="/assets/img/gallery/xabout.png.pagespeed.ic.NolByd-O8M.png" alt=""/>
                            </div>
                        </div>
                        <div className="col-xl-6 col-lg-6">
                            <div className="right-caption">

                                <div className="section-tittle section-tittle2 mb-50">
                                    <span>Our Top Services</span>
                                    <h2>Our Best Services</h2>
                                </div>
                                <div className="support-caption">
                                    <p className="pera-top">Mollit anim laborum duis adseu dolor iuyn voluptcate velit
                                        ess cillum
                                        dolore egru lofrre dsu quality mollit anim laborumuis au dolor in voluptate
                                        velit
                                        cillu.</p>
                                    <p className="mb-65">Mollit anim laborum.Dvcuis aute serunt iruxvfg dhjkolohr indd
                                        re voluptate
                                        velit esscillumlore eu quife nrulla parihatur. Excghcepteur sfwsignjnt occa
                                        cupidatat
                                        non aute iruxvfg dhjinulpadeserunt moll.</p>
                                    <a href="#" className="btn post-btn">More About Us</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div className="services-area section-padding3">
                <div className="container">
                    <div className="row">
                        <div className="cl-xl-7 col-lg-8 col-md-10">

                            <div className="section-tittle mb-70">
                                <span>Our Portfolios of cases</span>
                                <h2>Featured Case Study</h2>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-6 col-md-6 col-sm-10">
                            <div className="single-services mb-100">
                                <div className="services-img">
                                    <img src="/assets/img/gallery/xservices1.png.pagespeed.ic.KeLtQqAHr1.png" alt=""/>
                                </div>
                                <div className="services-caption">
                                    <span>Strategy planing</span>
                                    <p><a href="#">Within the construction industry as their overdraft</a></p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 col-md-6 col-sm-10">
                            <div className="single-services mb-100">
                                <div className="services-img">
                                    <img src="/assets/img/gallery/xservices2.png.pagespeed.ic.MInI9JOq2A.png" alt=""/>
                                </div>
                                <div className="services-caption">
                                    <span>Strategy planing</span>
                                    <p><a href="#">Within the construction industry as their overdraft</a></p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 col-md-6 col-sm-10">
                            <div className="single-services mb-100">
                                <div className="services-img">
                                    <img src="/assets/img/gallery/xservices3.png.pagespeed.ic.0VTTHj0rvE.png" alt=""/>
                                </div>
                                <div className="services-caption">
                                    <span>Strategy planing</span>
                                    <p><a href="#">Within the construction industry as their overdraft</a></p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 col-md-6 col-sm-10">
                            <div className="single-services mb-100">
                                <div className="services-img">
                                    <img src="/assets/img/gallery/xservices4.png.pagespeed.ic.HXQRZaODiv.png" alt=""/>
                                </div>
                                <div className="services-caption">
                                    <span>Strategy planing</span>
                                    <p><a href="#">Within the construction industry as their overdraft</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <TestimonialsAreaSection/>

            <CountdownAreaSection/>

            <div className="team-area section-padding30">
                <div className="container">
                    <div className="row">
                        <div className="cl-xl-7 col-lg-8 col-md-10">

                            <div className="section-tittle mb-70">
                                <span>Our Professional members </span>
                                <h2>Our Team Mambers</h2>
                            </div>
                        </div>
                    </div>
                    <div className="row">

                        <div className="col-xl-4 col-lg-4 col-md-6 col-sm-">
                            <div className="single-team mb-30">
                                <div className="team-img">
                                    <img src="/assets/img/gallery/xteam2.png.pagespeed.ic.ztMh4IVh87.png" alt=""/>
                                </div>
                                <div className="team-caption">
                                    <h3><a href="#">Ethan Welch</a></h3>
                                    <span>UX Designer</span>
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-4 col-lg-4 col-md-6 col-sm-">
                            <div className="single-team mb-30">
                                <div className="team-img">
                                    <img src="/assets/img/gallery/team3.png" alt=""/>
                                </div>
                                <div className="team-caption">
                                    <h3><a href="#">Ethan Welch</a></h3>
                                    <span>UX Designer</span>
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-4 col-lg-4 col-md-6 col-sm-">
                            <div className="single-team mb-30">
                                <div className="team-img">
                                    <img src="/assets/img/gallery/xteam1.png.pagespeed.ic.FQ42DN_v4L.png" alt=""/>
                                </div>
                                <div className="team-caption">
                                    <h3><a href="#">Ethan Welch</a></h3>
                                    <span>UX Designer</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <section className="wantToWork-area w-padding2 section-bg"
                     style={{backgroundImage: `url("assets/img/gallery/section_bg03.jpg.webp")`}}
            >
                <div className="container">
                    <div className="row align-items-center justify-content-between">
                        <div className="col-xl-7 col-lg-9 col-md-8">
                            <div className="wantToWork-caption wantToWork-caption2">
                                <h2>Are you Searching<br/> For a First-Class Consultant?</h2>
                            </div>
                        </div>
                        <div className="col-xl-2 col-lg-3 col-md-4">
                            <a href="#" className="btn btn-black f-right">More About Us</a>
                        </div>
                    </div>
                </div>
            </section>


            <div className="home-blog-area section-padding30">
                <div className="container">

                    <div className="row">
                        <div className="col-lg-12">
                            <div className="section-tittle mb-100">
                                <span>Recent News of us</span>
                                <h2>Our Recent Blog</h2>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xl-6 col-lg-6 col-md-6">
                            <div className="home-blog-single mb-30">
                                <div className="blog-img-cap">
                                    <div className="blog-img">
                                        <img src="/assets/img/gallery/xhome_blog1.png.pagespeed.ic.lBi-5Xxm6w.png"
                                             alt=""/>
                                        <ul>
                                            <li>By Admin - October 27, 2020</li>
                                        </ul>
                                    </div>
                                    <div className="blog-cap">
                                        <h3><a href="#">16 Easy Ideas to Use in Everyday</a></h3>
                                        <p>Amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                                            et
                                            dolore magnua Quis ipsum suspendisse ultrices gra.</p>
                                        <a href="#" className="more-btn">Read more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-6 col-lg-6 col-md-6">
                            <div className="home-blog-single mb-30">
                                <div className="blog-img-cap">
                                    <div className="blog-img">
                                        <img src="/assets/img/gallery/xhome_blog2.png.pagespeed.ic.8I1mcC3hCX.png"
                                             alt=""/>
                                        <ul>
                                            <li>By Admin - October 27, 2020</li>
                                        </ul>
                                    </div>
                                    <div className="blog-cap">
                                        <h3><a href="#">16 Easy Ideas to Use in Everyday</a></h3>
                                        <p>Amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                                            et
                                            dolore magnua Quis ipsum suspendisse ultrices gra.</p>
                                        <a href="#" className="more-btn">Read more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <BrandAreaSection/>
        </Layout>
    );
}

export default HomePageContent;
