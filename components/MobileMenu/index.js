import React, {Component} from 'react';
import Link from "next/link";

class MobileMenu extends Component {

    componentDidMount() {
        this.$menu = $(this.menu);

        this.$menu.slicknav({
            prependTo: '.mobile_menu',
            closedSymbol: '+',
            openedSymbol: '-'
        });
    }

    componentWillUnmount() {

    }

    render() {
        return (
            <div>
                <div className="mobile_menu d-block d-lg-none" ref={el => this.elContainer = el}/>
                <div style={{ maxHeight: '20px', overflowY: 'hidden'}}>
                    <ul id="navigation" ref={el => this.menu = el} className="d-block d-lg-none">
                        <li>
                            <Link href="/" passHref>
                                <a>Home</a>
                            </Link>
                        </li>
                        <li>
                            <Link href="/about" passHref>
                                <a>About</a>
                            </Link>
                        </li>
                        <li>
                            <Link href="/services" passHref>
                                <a>Services</a>
                            </Link>
                        </li>
                        <li>
                            <Link href="/contact" passHref>
                                <a>Contact</a>
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default MobileMenu;
