import React, {useEffect, useState} from 'react';
import Link from "next/link";

import MobileMenu from "../MobileMenu";

function Navbar() {
    const [scrolled, setScrolled] = useState(false);

    useEffect(() => {
        const setNavBackgroundChange = () => {
            window.pageYOffset > 200 ? setScrolled(true) : setScrolled(false);
        };

        window.addEventListener('scroll', setNavBackgroundChange);
        return () => window.removeEventListener('scroll', setNavBackgroundChange);
    }, []);

    return (
        <header>
            <div className="header-area">
                <div className="main-header">
                    <div className="header-top d-none d-lg-block">
                        <div className="container">
                            <div className="col-xl-12">
                                <div className="row d-flex justify-content-between align-items-center">
                                    <div className="header-info-left">
                                        <ul>
                                            <li><i className="far fa-clock"/> Mon - SAT: 6.00 am - 10.00 pm</li>
                                            <li>Sun: Closed</li>
                                        </ul>
                                    </div>
                                    <div className="header-info-right">
                                        <ul className="header-social">
                                            <li><a href="#"><i className="fab fa-facebook-f"/></a></li>
                                            <li><a href="#"><i className="fab fa-twitter"/></a></li>
                                            <li><a href="#"><i className="fab fa-linkedin-in"/></a></li>
                                            <li><a href="#"><i className="fab fa-google-plus-g"/></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className={`header-bottom header-sticky ${scrolled ? 'sticky-bar' : ''}`}>
                        <div className="container">
                            <div className="row align-items-center">

                                <div className="col-xl-2 col-lg-2">
                                    <div className="logo">
                                        <Link href="/" passHref>
                                            <a>
                                                <img
                                                    src="/assets/img/logo/xlogo.png.pagespeed.ic.1a99EduJ6Z.png"
                                                    alt=""/>
                                            </a>
                                        </Link>
                                    </div>
                                </div>
                                <div className="col-xl-10 col-lg-10">
                                    <div className="menu-wrapper  d-flex align-items-center justify-content-end">
                                        <div className="main-menu d-none d-lg-block">
                                            <nav>
                                                <ul id="navigation">
                                                    <li>
                                                        <Link href="/" passHref>
                                                            <a>Home</a>
                                                        </Link>
                                                    </li>
                                                    <li>
                                                        <Link href="/about" passHref>
                                                            <a>About</a>
                                                        </Link>
                                                    </li>
                                                    <li>
                                                        <Link href="/services" passHref>
                                                            <a>Services</a>
                                                        </Link>
                                                    </li>
                                                    <li>
                                                        <Link href="/contact" passHref>
                                                            <a>Contact</a>
                                                        </Link>
                                                    </li>
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-12">
                                    <MobileMenu />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    );
}

export default Navbar;
