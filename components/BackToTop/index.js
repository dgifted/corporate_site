import React, {useEffect, useState} from 'react';

const styles = {
    outline: 'none',
    backgroundColor: 'transparent',
    border: 'none',
};

function BackToTop() {
    const [visible, setVisible] = useState(false);

    const moveToTop = () => {
        if (window)
            window.scrollTo({
                top: 0,
                left: 0,
                behavior: "smooth"
            });
    };

    useEffect(() => {
        const setVisibility = (e) => {
            window.pageYOffset > 300 ? setVisible(true) : setVisible(false);
        };

        window.addEventListener('scroll', setVisibility);
        return () => window.removeEventListener('scroll', setVisibility);
    }, []);

    return (
        <>
            {visible
                ? (
                    <div id="back-top">
                        <button
                            style={styles}
                            title="Go to Top" onClick={moveToTop}>
                            <i className="fas fa-level-up-alt"/>
                        </button>
                    </div>
                )
                : null}
        </>
    );
}

export default BackToTop;
