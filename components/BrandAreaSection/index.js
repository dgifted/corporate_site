import React, {useMemo} from 'react';
import Slider from "react-slick";
import {useWindowSize} from "../../@hooks/use-window-size";

function BrandAreaSection({extraClasses}) {
    const {width} = useWindowSize();

    const setBrandSlideCount = useMemo(() => {
        return Math.floor(width / 175) < 5 ? Math.floor(width / 175) : 5;
    }, [width]);

    const brandSlidesSetting = {
        autoplay: true,
        arrows: false,
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: setBrandSlideCount,
        slidesToScroll: 1
    }

    return (
        <div className={`brand-area pb-140 ${!!extraClasses ? extraClasses : ''}`}>
            <div className="container">
                <div className="brand-active brand-border pb-40">
                    <Slider {...brandSlidesSetting}>
                        <div className="single-brand">
                            <img src="/assets/img/gallery/xbrand1.png.pagespeed.ic.shknrlwgMX.png" alt=""/>
                        </div>
                        <div className="single-brand">
                            <img src="/assets/img/gallery/xbrand2.png.pagespeed.ic.grbPxjb2H3.png" alt=""/>
                        </div>
                        <div className="single-brand">
                            <img src="/assets/img/gallery/xbrand3.png.pagespeed.ic.7BR8Swm9hI.png" alt=""/>
                        </div>
                        <div className="single-brand">
                            <img src="/assets/img/gallery/xbrand4.png.pagespeed.ic.1yMoSpAhZh.png" alt=""/>
                        </div>
                        <div className="single-brand">
                            <img src="/assets/img/gallery/xbrand2.png.pagespeed.ic.grbPxjb2H3.png" alt=""/>
                        </div>
                        <div className="single-brand">
                            <img src="/assets/img/gallery/xbrand5.png.pagespeed.ic.Icb8BVuKZx.png" alt=""/>
                        </div>
                    </Slider>
                </div>
            </div>
        </div>
    );
}

export default BrandAreaSection;
