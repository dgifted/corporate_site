import React from 'react';

import Layout from "../Layout";
import {useForm} from "react-hook-form";
import FormErrorMessage from "../FormErrorMessage";

function ContactPageContent() {
    const {register, handleSubmit, reset, formState: {errors}} = useForm();

    const onSubmit = (formPayload) => {
        console.log('formPayload: ', formPayload);
        reset();
    };

    return (
        <Layout title={`${process.env.siteName} | Contact Us`}>
            <div className="slider-area2">
                <div className="slider-height2 hero-overly2 d-flex align-items-center">
                    <div className="container">
                        <div className="row">
                            <div className="col-xl-12">
                                <div className="hero-cap hero-cap2 text-center">
                                    <h2>Contact US</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <section className="contact-section">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <h2 className="contact-title">Get in Touch</h2>
                        </div>
                        <div className="col-lg-8">
                            <form className="form-contact contact_form" id="contactForm" noValidate
                                  onSubmit={handleSubmit(onSubmit)}>
                                <div className="row">
                                    <div className="col-12">
                                        <div className="form-group">
                                            <textarea className="form-control w-100" name="message" id="message"
                                                      cols="30"
                                                      rows="9"
                                                      onFocus="this.placeholder = ''"
                                                      onBlur="this.placeholder = 'Enter Message'"
                                                      placeholder=" Enter Message"
                                                      {...register('message', {required: true, minLength: 10})}
                                            />
                                            {errors.message?.type === 'required' ?
                                                <FormErrorMessage message='Message is required.'/> : null}
                                            {errors.message?.type === 'minLength' ?
                                                <FormErrorMessage message='Message content should be at least 10 characters.'/> : null}
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <div className="form-group">
                                            <input className="form-control valid" name="name" id="name" type="text"
                                                   onFocus="this.placeholder = ''"
                                                   onBlur="this.placeholder = 'Enter your name'"
                                                   placeholder="Enter your name"
                                                   {...register('name', {required: true})}
                                            />
                                            {errors.name?.type === 'required' ?
                                                <FormErrorMessage message='Name is required.'/> : null}
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <div className="form-group">
                                            <input className="form-control valid" name="email" id="email" type="email"
                                                   onFocus="this.placeholder = ''"
                                                   onBlur="this.placeholder = 'Enter email address'"
                                                   placeholder="Email"
                                                   {...register('email', {
                                                       required: true,
                                                       pattern: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                                                   })}
                                            />
                                            {errors.email?.type === 'required' ?
                                                <FormErrorMessage message='Email is required.'/> : null}
                                        </div>
                                    </div>
                                    <div className="col-12">
                                        <div className="form-group">
                                            <input className="form-control" name="subject" id="subject" type="text"
                                                   onFocus="this.placeholder = ''"
                                                   onBlur="this.placeholder = 'Enter Subject'"
                                                   placeholder="Enter Subject"
                                                   {...register('subject')}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group mt-3">
                                    <button type="submit" className="button button-contactForm boxed-btn">Send</button>
                                </div>
                            </form>
                        </div>
                        <div className="col-lg-3 offset-lg-1">
                            <div className="media contact-info">
                                <span className="contact-info__icon"><i className="ti-home"/></span>
                                <div className="media-body">
                                    <h3>Buttonwood, California.</h3>
                                    <p>Rosemead, CA 91770</p>
                                </div>
                            </div>
                            <div className="media contact-info">
                                <span className="contact-info__icon"><i className="ti-tablet"/></span>
                                <div className="media-body">
                                    <h3>+1 253 565 2365</h3>
                                    <p>Mon to Fri 9am to 6pm</p>
                                </div>
                            </div>
                            <div className="media contact-info">
                                <span className="contact-info__icon"><i className="ti-email"/></span>
                                <div className="media-body">
                                    <h3><a href="https://preview.colorlib.com/cdn-cgi/l/email-protection"
                                           className="__cf_email__"
                                           data-cfemail="bfcccacfcfd0cdcbffdcd0d3d0cdd3d6dd91dcd0d2">support@oursite.ng</a>
                                    </h3>
                                    <p>Send us your query anytime!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </Layout>
    );
}

export default ContactPageContent;
