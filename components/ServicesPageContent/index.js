import React from 'react';

import Layout from "../Layout";
import BrandAreaSection from "../BrandAreaSection";

function ServicesPageContent() {
    return (
        <Layout title={`${process.env.siteName} | Our services`}>
            <div className="slider-area2">
                <div className="slider-height2 hero-overly2 d-flex align-items-center">
                    <div className="container">
                        <div className="row">
                            <div className="col-xl-12">
                                <div className="hero-cap hero-cap2 text-center">
                                    <h2>Our Services</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="categories-area section-padding30">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">

                            <div className="section-tittle mb-70">
                                <span>Our Top Services</span>
                                <h2>Our Best Services</h2>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-4 col-md-6 col-sm-6">
                            <div className="single-cat text-center mb-50">
                                <div className="cat-icon">
                                    <span className="flaticon-development"/>
                                </div>
                                <div className="cat-cap">
                                    <h5><a href="#">Strategy Planning </a></h5>
                                    <p>There are many variations of passages of lorem Ipsum available but the new
                                        majority have
                                        suffered.</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-6">
                            <div className="single-cat text-center mb-50">
                                <div className="cat-icon">
                                    <span className="flaticon-result"/>
                                </div>
                                <div className="cat-cap">
                                    <h5><a href="#">Insurance Service</a></h5>
                                    <p>There are many variations of passages of lorem Ipsum available but the new
                                        majority have
                                        suffered.</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-6">
                            <div className="single-cat text-center mb-50">
                                <div className="cat-icon">
                                    <span className="flaticon-team"/>
                                </div>
                                <div className="cat-cap">
                                    <h5><a href="#">Audit & Evaluation</a></h5>
                                    <p>There are many variations of passages of lorem Ipsum available but the new
                                        majority have
                                        suffered.</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-6">
                            <div className="single-cat text-center mb-50">
                                <div className="cat-icon">
                                    <span className="flaticon-result"/>
                                </div>
                                <div className="cat-cap">
                                    <h5><a href="#">Insurance Service</a></h5>
                                    <p>There are many variations of passages of lorem Ipsum available but the new
                                        majority have
                                        suffered.</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-6">
                            <div className="single-cat text-center mb-50">
                                <div className="cat-icon">
                                    <span className="flaticon-team"/>
                                </div>
                                <div className="cat-cap">
                                    <h5><a href="#">Audit & Evaluation</a></h5>
                                    <p>There are many variations of passages of lorem Ipsum available but the new
                                        majority have
                                        suffered.</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-6">
                            <div className="single-cat text-center mb-50">
                                <div className="cat-icon">
                                    <span className="flaticon-development"/>
                                </div>
                                <div className="cat-cap">
                                    <h5><a href="#">Strategy Planning </a></h5>
                                    <p>There are many variations of passages of lorem Ipsum available but the new
                                        majority have
                                        suffered.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <BrandAreaSection/>
        </Layout>
    );
}

export default ServicesPageContent;
