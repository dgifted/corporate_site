import React from 'react';

function FormErrorMessage({message}) {
    return (
        <i className="text-danger" style={{ fontSize: '0.9rem', color: '#ff002b'}} role="alert">{message}</i>
    );
}

export default FormErrorMessage;
