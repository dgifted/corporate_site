import React, {useEffect, useState} from 'react';
import VisibilitySensor from "react-visibility-sensor";
import CountUp from "react-countup";

function CountdownAreaSection() {
    const [mounted, setMounted] = useState(false);

    useEffect(() => {
        setMounted(true);
    }, []);

    return (
        <div className="count-down-area pb-120">
            <div className="container">
                <div className="row justify-content-between">
                    <div className="col-lg-3 col-md-6 col-sm-6">

                        <VisibilitySensor partialVisibility offset={{bottom: 200}}>
                            {({isVisible}) => (
                                <div className="single-counter text-center">
                                    {isVisible && mounted ? <CountUp start={1} end={8705}/> : null}
                                    <p>Projects Completed</p>
                                </div>
                            )}
                        </VisibilitySensor>

                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6">
                        <VisibilitySensor partialVisibility offset={{bottom: 200}}>
                            {({isVisible}) => (
                                <div className="single-counter active text-center">
                                    {isVisible && mounted ? <CountUp start={1} end={480}/> : null}
                                    <p> Active Clients</p>
                                </div>
                            )}
                        </VisibilitySensor>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6">
                        <VisibilitySensor partialVisibility offset={{bottom: 200}}>
                            {({isVisible}) => (
                                <div className="single-counter text-center">
                                    {isVisible && mounted ? <CountUp start={1} end={626}/> : null}
                                    <p>Cups of Coffee</p>
                                </div>
                            )}
                        </VisibilitySensor>

                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-6">
                        <VisibilitySensor partialVisibility offset={{bottom: 200}}>
                            {({isVisible}) => (
                                <div className="single-counter text-center">
                                    {isVisible && mounted ? <CountUp start={1} end={9774}/> : null}
                                    <p>Happy Clients</p>
                                </div>
                            )}
                        </VisibilitySensor>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default CountdownAreaSection;
