const nextConfig = {
  reactStrictMode: true,
  env: {
    mobileLine: '',
    officeAddress: '',
    siteName: 'Ignite Bloom',
    supportEmail: ''
  }
}

module.exports = nextConfig
