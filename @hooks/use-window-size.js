import {useEffect, useState} from "react";

export const useWindowSize = () => {
    const [dimension, setDimension] = useState({width: 0, height: 0});

    useEffect(() => {
        setDimension(prevDimension => ({
            ...prevDimension,
            width: window.innerWidth,
            height: window.innerHeight
        }))
    }, []);

    return dimension;

}
